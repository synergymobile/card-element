# CardElement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.7.

This is a representation of web component. 

You can run project with `ng serve`.
You can build project with: `npm run build:elements`, which will then create build files in elements folder. 
After that, you can access elements folder, and push changes, so that they are accessible through npm installation on any project.

