/**
 * Created by Nemanja Zoric on 31/03/2021
 */
const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/card-element/runtime.js',
    './dist/card-element/polyfills.js',
    './dist/card-element/main.js',
  ];
  await fs.ensureDir('elements');
  await concat(files, 'elements/card-element.js');

  const styles = [
    './dist/card-element/styles.css',
  ]
  await concat(styles, 'elements/card-element.css');
})();
