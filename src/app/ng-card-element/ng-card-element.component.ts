import {
  Component,
  Input,
  Output,
  ViewEncapsulation,
  EventEmitter,
  OnChanges,
  SimpleChanges, ChangeDetectorRef,
} from '@angular/core';
import {NgCardElementService} from './service/ng-card-element.service';

export enum ProjectTypeEnum {
  webUi = 'webUi',
  coverApp = 'coverApp',
  managerApp = 'managerApp'
}

@Component({
  selector: 'app-ng-card-element',
  templateUrl: './ng-card-element.component.html',
  styleUrls: ['./ng-card-element.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class NgCardElementComponent implements OnChanges {

  @Input() public title: string;
  @Input() public name: string;
  @Input() public caption: string;
  @Input() public image: string;
  @Input() public avatar: string;
  @Input() public project: ProjectTypeEnum;

  public projectType: any;

  @Output() private likeNotify = new EventEmitter<any>();
  @Output() private shareNotify = new EventEmitter<any>();
  @Output() private commentNotify = new EventEmitter<any>();

  public constructor(private cdr: ChangeDetectorRef, private ngCardElementService: NgCardElementService) {
    this.projectType = ProjectTypeEnum;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.title = changes.title.currentValue;
    this.name = changes.name.currentValue;
    this.caption = changes.caption.currentValue;
    this.image = changes.image.currentValue;
    this.avatar = changes.avatar.currentValue;
    this.project = changes.project.currentValue;
    console.log(this.project, this.projectType.webUi);
    this.cdr.detectChanges();
  }

  public likeEvent() {
    this.likeNotify.emit('Like clicked');
  }

  public shareEvent() {
    this.shareNotify.emit('Sharing image...');
  }

  public async commentEvent() {
    const comments = await this.ngCardElementService.getComments();
    this.commentNotify.emit(comments);
  }

}
